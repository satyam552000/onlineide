import React from 'react';

export function Login(props) {
    const handelLogin = () => {
        props.handelLogin();
    };
    return (
        <div className="flex flex-col h-full justify-center items-center ">
            <div className=" rounded flex flex-col border-2 text-center shadow-lg  p-4">
                <p className="text-2xl mx-6 my-6">Login to continue</p>
                <button
                    onClick={handelLogin}
                    className="bg-green-300 mb-4  text-white text-lg p-2 py-1 rounded mt-6"
                >
                    Login
                </button>
            </div>
        </div>
    );
}
