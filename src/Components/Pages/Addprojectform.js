import React, { useState } from 'react';

export function Addprojectform(props) {
    const [name, setName] = useState('');
    const [language, setLanguage] = useState('');
    const handelSubmit = (e) => {
        e.preventDefault();
        if (language === 'c++' || language === 'java') {
            props.addProject(name, language);
            setName('');
            setLanguage('');
        }
    };
    return (
        <form className=" flex mt-auto flex-col" onSubmit={handelSubmit}>
            <label htmlFor="name" className="text-xs">
                Name
            </label>
            <input
                id="name"
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                className="border-2"
            />

            <label htmlFor="language" className="text-xs">
                Language (c++/java)
            </label>
            <input
                id="language"
                type="text"
                value={language}
                onChange={(e) => setLanguage(e.target.value)}
                className="border-2"
            />
            <button>Create</button>
        </form>
    );
}
