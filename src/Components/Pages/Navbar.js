import React from 'react';

export function Navbar() {
    return (
        <div className="bg-gray-700 shadow-md p-4 items-center  text-white flex  ">
            <div className="w-full mx-auto flex md:w-10/12">
                <div className="text-3xl tracking-wider ">IDE</div>
                <button className="hover:text-gray-200 ml-auto">Logout</button>
            </div>
        </div>
    );
}
