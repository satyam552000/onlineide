import React, { Component } from 'react';
import { Navbar } from './Navbar';
import { Addprojectform } from './Addprojectform';
export default class Homepage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            user: props.user,
            projects: [...props.user.getprojects()],
            currentproject: -1,
            programs: [],
            currentprogram: -1,
            code: '',
            input: '',
            output: '',
        };
        this.addProject = this.addProject.bind(this);
        this.handelProgramDelete = this.handelProgramDelete.bind(this);
        this.handelProgramcreation = this.handelProgramcreation.bind(this);
        this.updatestates = this.updatestates.bind(this);
        this.handelRun = this.handelRun.bind(this);
    }
    addProject(name, language) {
        // console.log(this.state.user);
        this.state.user.createProject(name, language);
        this.updatestates();
    }
    updatestates() {
        this.setState({ projects: [...this.state.user.getprojects()] }, () => {
            if (this.state.projects.length > 0)
                this.setState({
                    programs: this.state.projects[0].getPrograms(),
                    currentproject: 0,
                    currentprogram: 0,
                });
        });
    }
    componentDidMount() {
        if (this.state.projects.length > 0)
            this.setState({
                programs: this.state.projects[0].getPrograms(),
                currentproject: 0,
            });
    }
    handelProgramDelete(index) {
        const { projects, programs, currentproject } = this.state;
        if (programs.length > 1 && currentproject !== -1) {
            projects[currentproject].delProgram(index);
            // console.log(projects[currentproject]);
            this.setState({
                projects: this.state.user.getprojects(),
                programs: projects[currentproject].getPrograms(),
                currentprogram: 0,
            });
        }
    }
    handelProgramcreation() {
        const { projects, currentproject } = this.state;
        if (currentproject !== -1) {
            projects[currentproject].createProgram();
            this.setState({
                projects: this.state.user.getprojects(),
                programs: projects[currentproject].getPrograms(),
                currentprogram: this.state.currentprogram + 1,
            });
        }
    }
    async handelRun() {
        const { programs, currentprogram } = this.state;
        if (currentprogram !== -1) {
            programs[currentprogram].setcode(this.state.code);
            this.setState({ output: 'loading...' });
            const res = await programs[currentprogram].run(this.state.input);
            this.setState({ output: res });
        }
    }
    async handelSave() {
        const { programs, currentprogram } = this.state;
        if (currentprogram !== -1) {
            programs[currentprogram].setcode(this.state.code);
        }
    }
    componentDidUpdate(prevProps, prevState) {
        // console.log(prevState);
        const { programs, currentprogram } = prevState;
        if (currentprogram >= programs.length) {
            this.setState({ currentprogram: 0, code: programs[0].getcode() });
        } else if (currentprogram !== this.state.currentprogram) {
            if (currentprogram !== -1) {
                programs[currentprogram].setcode(this.state.code);
                this.setState({
                    code: programs[this.state.currentprogram].getcode(),
                });
            }
        }
    }
    render() {
        const { projects, programs, code, input, currentprogram, output } =
            this.state;
        return (
            <div className="h-full">
                <Navbar />
                <div className=" mt-4 mx-auto flex w-full md:w-11/12 h-4/5 ">
                    <div className="h-full hidden md:flex md:flex-col w-2/12 mt-0 m-2 p-2 border-2">
                        {projects &&
                            projects.map((proj, index) => {
                                return (
                                    <div
                                        onClick={() =>
                                            this.setState({
                                                currentproject: index,
                                            })
                                        }
                                        key={proj.name}
                                        className="border-b-2 p-1"
                                    >
                                        {proj.name}
                                    </div>
                                );
                            })}
                        <Addprojectform addProject={this.addProject} />
                    </div>
                    <div className="w-8/12 border-2 h-full flex flex-col  mr-1">
                        <div className="w-full  flex h-12  overflow-x-auto">
                            {programs.map((prog, index) => (
                                <div
                                    key={index}
                                    onClick={() =>
                                        this.setState({
                                            currentprogram: index,
                                        })
                                    }
                                    className={
                                        currentprogram === index
                                            ? 'bg-gray-200 cursor-pointer rounded-md w-2/12 h-8 p-1  mr-2 inline-block whitespace-nowrap overflow-y-hidden scrollhidden  overflow-x-auto'
                                            : 'bg-gray-400 hover:bg-gray-200 cursor-pointer rounded-md w-2/12 h-8 p-1  mr-2 inline-block whitespace-nowrap overflow-y-hidden scrollhidden  overflow-x-auto'
                                    }
                                >
                                    Tab {index + 1}
                                    <button
                                        className="float-right text-white mr-1"
                                        onClick={(e) => {
                                            this.handelProgramDelete(index);
                                        }}
                                        data-testid="closeprogram"
                                    >
                                        X
                                    </button>
                                </div>
                            ))}

                            <div
                                data-testid="addprogram"
                                onClick={this.handelProgramcreation}
                                className="cursor-pointer bg-gray-400 text-white leading-4 text-xl pb-0.5  center text-center rounded-sm w-10 h-8 p-1  mr-2 inline-block whitespace-nowrap overflow-y-hidden scrollhidden  overflow-x-auto"
                            >
                                +
                            </div>
                        </div>
                        <textarea
                            id="codearea"
                            placeholder="start coding"
                            value={code}
                            onChange={(e) =>
                                this.setState({ code: e.target.value })
                            }
                            className="p-2 pb-4  bo  w-full h-full box-border text-lg resize-none outline-none"
                        />
                        <div className=" justify-evenly leading-4 flex relative bottom-0  w-3/12 ml-auto">
                            <button
                                onClick={this.handelRun}
                                id="runbutton"
                                className="p-2 w-14 py-0.5 text-white rounded bg-blue-500"
                            >
                                Run
                            </button>
                            <button
                                onClick={this.handelSave}
                                className="p-2 border-green-400 border-2 text-green-400 rounded"
                            >
                                Save
                            </button>
                        </div>
                    </div>
                    <div className=" h-full w-4/12">
                        <div className=" h-3/6 flex flex-col border-2 p-2">
                            <label htmlFor="input" className="text-lg">
                                Input
                            </label>
                            <textarea
                                id="input"
                                value={input}
                                onChange={(e) =>
                                    this.setState({ input: e.target.value })
                                }
                                className="w-full  h-full resize-none outline-none "
                            />
                        </div>
                        <div className=" h-3/6 flex flex-col  border-2 p-2">
                            <label htmlFor="output" className="text-lg">
                                Output
                            </label>
                            <textarea
                                id="output"
                                value={output}
                                onChange={(e) =>
                                    this.setState({ output: e.target.value })
                                }
                                className="w-full h-full resize-none outline-none "
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
