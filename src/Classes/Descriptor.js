class Descriptor {
    constructor(data, project) {
        this.project = project;
        this.language = [];
        for (let i = 0; i < data.length; i++) {
            this.language.push(data[i].language);
        }
        this.language = data.language;
    }

    getLanguage(index) {
        if (index < this.language.length) {
            return this.language[index].language;
        } else return new Error('Index out of range');
    }
    deLanguage(index) {
        if (index < this.language.length) {
            this.language.splice(index, 1);
        } else return new Error('Index out of range');
    }
    addLanguage(lang) {
        this.language.push(lang);
        return this.language.size - 1;
    }
    getCurrentlength() {
        return this.language.length;
    }
}

export { Descriptor };
