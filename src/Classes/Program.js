class Program {
    constructor(obj) {
        this.code = obj.code;
        this.language = obj.language;
    }

    getcode() {
        return this.code;
    }
    setcode(code) {
        this.code = code;
    }
    run(input) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                return resolve('compiled successfully');
            }, 1000);
        });
    }
    getoutput() {
        // get output from backend
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                return resolve('compiled successfully');
            }, 1000);
        });
    }
}

export { Program };
