import { Program } from './Program';
import { Descriptor } from './Descriptor';

class Project {
    constructor(name, language, data) {
        this.name = name;
        this.programs = [];

        this.language = language;
        this.Descriptor = new Descriptor(data, this);
        for (let i = 0; i < data.length; i++) {
            let p = new Program(data[i]);
            this.programs.push(p);
        }
        if (this.programs.length === 0) this.createProgram();
    }
    getName() {
        return this.name;
    }
    rename(name) {
        return (this.name = name);
    }
    getPrograms() {
        return this.programs;
    }
    createProgram() {
        let language = this.language;
        let newProgram = new Program({ code: '', language });
        this.programs.push(newProgram);
        return this.programs.length - 1;
    }
    delProgram(index) {
        if (index < this.programs.length && this.programs.length > 1) {
            this.programs.splice(index, 1);
        } else {
            return new Error('Array Index Out Of range');
        }
    }
}
export { Project };
