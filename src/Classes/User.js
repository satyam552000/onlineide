import { Project } from './Project';

class User {
    constructor(id, name, token) {
        this.id = id;
        this.name = name;
        this.token = token;
        this.getUserdata();
        this.projects = [];
    }
    getprojects() {
        return this.projects;
    }
    async getUserdata() {
        //gets user data from backend
    }
    delProject(index) {
        if (index < this.projects.length && index > 0) {
            this.projects.splice(index, 1);
        } else {
            return new Error(`index out of bound got ${index} `);
        }
    }
    createProject(name, language) {
        let newProject = new Project(name, language, []);
        this.projects.push(newProject);
        return this.projects.length - 1;
    }
}
export { User };
