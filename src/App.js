import React from 'react';
import { Login, Homepage } from './Components';
import { User } from './Classes/User';
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            logedin: false,
            user: null,
        };
        this.login = this.login.bind(this);
    }
    login(data) {
        const dummy = {
            id: 1234,
            name: 'satyam',
            token: '',
        };
        this.setState({ user: new User(dummy) });
        this.setState({ logedin: true });
    }
    logout(data) {
        this.setState({ logedin: false, user: null });
    }

    render() {
        if (this.state.logedin)
            return <Homepage logout={this.logout} user={this.state.user} />;

        return <Login handelLogin={this.login} />;
    }
}

export default App;
