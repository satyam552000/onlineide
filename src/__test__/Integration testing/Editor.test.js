import { render, screen, fireEvent } from '@testing-library/react';
import App from '../../App';
import { User } from '../../Classes/User';
import { Homepage } from '../../Components';

const originalError = console.error;

describe('Editor Tests', () => {
    beforeAll(() => {
        console.error = (...args) => {
            if (/Warning.*not wrapped in act/.test(args[0])) {
                return;
            }
            originalError.call(console, ...args);
        };
    });
    afterAll(() => {
        console.error = originalError;
    });
    test('should log in user on Click login', () => {
        render(<App />);
        //login
        const login = screen.getByRole('button', { name: 'Login' });
        fireEvent.click(login);

        const title = screen.getByText(/IDE/);
        expect(title).toBeInTheDocument();
    });

    test('should create a new project', () => {
        const dummy = {
            id: 1234,
            name: 'satyam',
            token: '',
        };
        render(<Homepage user={new User(dummy)} />);

        //create a new project
        createnewproject({ name: 'testproject', language: 'c++' });

        const projectname = screen.getByText('testproject');
        expect(projectname).toBeInTheDocument();
    });
    test('should create a new program once project is created', () => {
        const dummy = {
            id: 1234,
            name: 'satyam',
            token: '',
        };
        render(<Homepage user={new User(dummy)} />);

        createnewproject({ name: 'testproject', language: 'c++' });
        const programtitle = screen.getByText('Tab 1');
        expect(programtitle).toBeInTheDocument();
    });
    test('should add tabs only when project is created', () => {
        render(<App />);
        //login the system
        login();

        //try to add program
        const addbutton = screen.getByTestId('addprogram');
        fireEvent.click(addbutton);

        //expect no program creation since no project
        const tab1 = screen.queryByText('Tab 1');
        expect(tab1).not.toBeInTheDocument();

        //create a new project
        createnewproject({ name: 'testproject', language: 'c++' });

        //check that now program should be created
        const programtitle = screen.getByText('Tab 1');
        expect(programtitle).toBeInTheDocument();

        //trying to create a new program
        fireEvent.click(addbutton);
        const programtitle2 = screen.getByText('Tab 2');

        expect(programtitle2).toBeInTheDocument();
    });
    test('should switch code content while switching tabs', () => {
        render(<App />);
        //login in the system;
        login();

        //create a new project
        createnewproject({ name: 'testproject', language: 'c++' });

        //create a new tab
        const addbutton = screen.getByTestId('addprogram');
        fireEvent.click(addbutton);

        const tab1 = screen.getByText('Tab 1');
        const tab2 = screen.getByText('Tab 2');
        const codearea = screen.getByPlaceholderText('start coding');

        fireEvent.click(tab1);
        fireEvent.change(codearea, { target: { value: 'tab1' } });
        fireEvent.click(tab2);
        expect(codearea).toHaveValue('');
        fireEvent.change(codearea, { target: { value: 'tab2' } });
        expect(codearea).toHaveValue('tab2');
        fireEvent.click(tab1);
        expect(codearea).toHaveValue('tab1');
    });
    test('should display output when code is run', async () => {
        render(<App />);
        //login in the system;
        login();

        //create a new project
        createnewproject({ name: 'testproject', language: 'c++' });

        //create a new tab

        const codearea = screen.getByPlaceholderText('start coding');

        fireEvent.change(codearea, { target: { value: 'print("hello");' } });

        const runbutton = screen.getByRole('button', { name: 'Run' });
        expect(runbutton).toBeInTheDocument();
        fireEvent.click(runbutton);
        const outputscreen = await screen.findByLabelText('Output');
        expect(outputscreen).toHaveValue('loading...');
    });
});

const createnewproject = ({ name, language }) => {
    const formname = screen.getByLabelText('Name');
    const formlanguage = screen.getByLabelText(`Language (c++/java)`);
    const submit = screen.getByRole('button', { name: /Create/ });
    fireEvent.change(formname, { target: { value: name } });
    fireEvent.change(formlanguage, { target: { value: language } });
    fireEvent.click(submit);
};

const login = () => {
    const login = screen.getByRole('button', { name: 'Login' });
    fireEvent.click(login);
};
