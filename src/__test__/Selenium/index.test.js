import { Builder, By, Key, until } from 'selenium-webdriver';

const originalError = console.error;

describe('Component', () => {
    jest.setTimeout(30000);
    beforeAll(() => {
        console.error = (...args) => {
            if (/Warning.*not wrapped in act/.test(args[0])) {
                return;
            }
            originalError.call(console, ...args);
        };
    });
    afterAll(() => {
        console.error = originalError;
    });
    test('selenium testing', async () => {
        await loginandtypecode();
    });
});

export async function loginandtypecode() {
    let driver = await new Builder().forBrowser('chrome').build();
    try {
        // Navigate to Url
        await driver.get('http://localhost:3000');
        await driver.wait(until.elementLocated(By.css('button')), 10000);
        // Enter text "cheese" and perform keyboard action "Enter"

        await driver.findElement(By.css('button')).click();

        let firstResult = await driver.wait(
            until.elementLocated(By.css('#name')),
            10000
        );
        await driver.sleep(100);
        await driver
            .findElement(By.css('#name'))
            .sendKeys('Project1', Key.ENTER);
        await driver
            .findElement(By.css('#language'))
            .sendKeys('c++', Key.ENTER);
        await driver.findElement(By.css('#runbutton')).click();
        await driver.sleep(1000);
        await driver
            .findElement(By.css('#codearea'))
            .sendKeys('print("hello");', Key.ENTER);
        await driver.findElement(By.css('#input')).click();

        let first = await driver.wait(
            until.elementLocated(By.css('#output')),
            10000
        );
        await driver.sleep(3000);
        const text = await first.getAttribute('textContent');
        await driver.sleep(3000);

        expect.assertions(text === 'compiled successfully');
    } catch (e) {
    } finally {
        await driver.quit();
    }
}
