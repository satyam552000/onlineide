import { render, screen, fireEvent } from '@testing-library/react';
import { User } from '../../Classes/User';
import { Homepage } from '../../Components';

const originalError = console.error;

describe('Code area Component', () => {
    beforeAll(() => {
        console.error = (...args) => {
            if (/Warning.*not wrapped in act/.test(args[0])) {
                return;
            }
            originalError.call(console, ...args);
        };
    });
    afterAll(() => {
        console.error = originalError;
    });
    test('renders Code area and User can type init', () => {
        const dummy = {
            id: 1234,
            name: 'satyam',
            token: '',
        };
        const user = new User(dummy);
        render(<Homepage user={user} />);
        // const loginbutton = screen.getByText(/Login/i);
        const codearea = screen.getByPlaceholderText('start coding');
        expect(codearea).toBeInTheDocument();

        fireEvent.change(codearea, { target: { value: 'print("hello");' } });
        const code = screen.getByText('print("hello");');
        expect(code).toBeInTheDocument();
    });
    test('renders input field and user can type init', () => {
        const dummy = {
            id: 1234,
            name: 'satyam',
            token: '',
        };
        const user = new User(dummy);
        render(<Homepage user={user} />);
        // const loginbutton = screen.getByText(/Login/i);
        const Input = screen.getByLabelText('Input');
        expect(Input).toBeInTheDocument();

        fireEvent.change(Input, { target: { value: 'test123' } });

        expect(Input).toHaveValue('test123');
    });
    test('renders output field and user can type init', () => {
        const dummy = {
            id: 1234,
            name: 'satyam',
            token: '',
        };
        const user = new User(dummy);
        render(<Homepage user={user} />);
        // const loginbutton = screen.getByText(/Login/i);
        const Output = screen.getByLabelText('Output');
        expect(Output).toBeInTheDocument();

        fireEvent.change(Output, { target: { value: 'test123' } });

        expect(Output).toHaveValue('test123');
    });
});
