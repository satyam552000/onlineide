import { render, screen } from '@testing-library/react';
import { Login } from '../../Components';

const originalError = console.error;

describe('login Component', () => {
    beforeAll(() => {
        console.error = (...args) => {
            if (/Warning.*not wrapped in act/.test(args[0])) {
                return;
            }
            originalError.call(console, ...args);
        };
    });
    afterAll(() => {
        console.error = originalError;
    });
    test('renders Login button', () => {
        const fn = jest.fn();
        render(<Login handelLogin={fn} />);
        // const loginbutton = screen.getByText(/Login/i);
        const login = screen.getByRole('button', { name: 'Login' });
        expect(login).toBeInTheDocument();
    });
});
